public class Jackpot{
	public static void main(String[]args){
		System.out.println("Welcome to the jackpot game!");
		System.out.println("You will test you're luck and see if you can land on the jackpot.");
		Board game = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while (!gameOver){
			System.out.println(game);
			//Check if the word is found
			if (game.playATurn()){
				gameOver = true;
			}
			else
				numOfTilesClosed+=1;		
		}
		if (numOfTilesClosed >= 7){
			System.out.println("Good Job! You won the Jackpot!");
		}
		else {
			System.out.println("You lost...");
	}
}
}