import java.util.Random;
public class Die{
	
	private Random rand;
	private int faceValue;
	
	public Die(){
		this.rand = new Random();
		this.faceValue=1;
	}
	public int getFaceValue(){
		return this.faceValue;
	}
	public void roll(){
		this.faceValue = rand.nextInt(6)+1;
	}
	public String toString(){
		return ""+this.faceValue;
	}
}
	
	