public class Board{
	
	private Die diceOne;
	private Die diceTwo;
	
	private boolean[] tiles;
	private String[] letters;
	
	public Board(){
		this.diceOne = new Die();
		this.diceTwo = new Die();
		this.tiles = new boolean[12];
	}
	
	
	
	public String toString(){
		String line ="";
		for (int i =0;i< tiles.length ;i++){
			if (tiles[i]){
				line += "X"+" ";
			}else 
				line += (i+1)+" ";
			}
		return line ;
		}
	
	public boolean playATurn(){
		
		diceOne.roll();
		diceTwo.roll();
		System.out.println(diceOne);
		System.out.println(diceTwo);
		int sumOfDice=diceOne.getFaceValue()+diceTwo.getFaceValue();
		
		//SUM OF DICE
		
		if (!tiles[sumOfDice-1]){
			tiles[sumOfDice-1]=true;
			System.out.println("Closing the tile equal to sum: "+sumOfDice);
			return false;
		}
		
		//DICE ONE
		else if (!tiles[diceOne.getFaceValue()-1]){
			tiles[diceOne.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as die one: "+diceOne.getFaceValue());
			return false;
		}
		//DICE TWO
		else if (!tiles[diceTwo.getFaceValue()-1]){
				tiles[diceTwo.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as die one: "+diceTwo.getFaceValue());
			return false;
		}
		else 
			System.out.println("All the tiles for these values are already shut");
			return true;
	}
}
				
	
	
			
	
	
	